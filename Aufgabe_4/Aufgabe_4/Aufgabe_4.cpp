#include <iostream>
#include <cstdlib>
#include <queue>
#include <random>

using namespace std;

#define MAX_ELEMENTS 6
#define MAX_VALUE 100


//Die Klasse TREE enth�lt die Baumstruktur
class TREE
{
	int data;
	TREE *left_next = 0;
	TREE *right_next = 0;

public:

	TREE() {}

	TREE(int in)
	{
		data = in;
	}
	void set_left(int nextElement)
	{
		left_next = new TREE(nextElement);
	}

	void set_right(int nextElement)
	{
		right_next = new TREE(nextElement);
	}

	void set_data(int i)
	{
		data = i;
	}

	TREE* get_left()
	{
		return left_next;
	}

	TREE* get_right()
	{
		return right_next;
	}

	int get_content()
	{
		return data;
	}

	void print_element()
	{
		cout << data << endl;
	}

};

//Erstellt Zufallszahl
int get_random()
{
	random_device random;
	uniform_int_distribution<int> distribution(0, MAX_VALUE);
	return distribution(random);
}

//laesst neues Element durch den Baum sickern
void heapify(TREE* first, int newElement)
{
	TREE *node = first;

	//Wurzel ist nicht NULL
	if (node->get_content() != 0)
	{
		//neues Element ist kleiner als Wurzel
		if (newElement < node->get_content())
		{
			//linker Knoten existiert
			if (node->get_left() != 0)
			{
				heapify(node->get_left(), newElement);
			}
			//linker Knoten existiert nicht
			else
			{
				node->set_left(newElement);
			}
		}

		//neues Element ist groesser/gleich als Wurzel
		else
		{
			if (node->get_right() != 0)
			{
				heapify(node->get_right(), newElement);
			}
			else
			{
				node->set_right(newElement);
			}
		}
	}
	//Wurzel ist NULL
	else
	{
		node->set_data(newElement);
	}

}
//Erstellt neuen Baum mit 'size'-vielen Zufallszahlen
void create_tree(TREE* first, int size)
{
	for (int i = 0; i < size; i++)
	{
		int random = get_random();
		heapify(first, random);
	}
}

void create_tree(TREE* first, int size, int* array)
{
	for (int i = 0; i < size; i++)
	{
		int in = array[i];
		heapify(first, in);
	}
}

//gibt Baum in inorder aus
void inorder(TREE* first)
{
	if (first != NULL) {

		if (first->get_left()) inorder(first->get_left());
		cout << "- " << first->get_content() << endl;
		if (first->get_right()) inorder(first->get_right());
	}
}

void print_tree(TREE* first)
{
	cout << "Baum wird in inorder-Reihenfolge ausgegeben: " << endl;
	inorder(first);
}

TREE* contains(TREE* first, int x)
{
	TREE* current = first;
	while (current != NULL)
	{
		if (current->get_content() == x)
		{
			return current;
		}
		else
		{
			if (x < current->get_content())
			{
				current = current->get_left();
			}
			else
			{
				current = current->get_right();
			}
		}
	}

	return 0;
}

void delete_node(TREE* first, int element)
{
	TREE* to_delete = contains(first, element);
	TREE* current = to_delete;
	//Element existiert im Baum
	if (contains(first, element) != 0)
	{
		//1.Fall: Knoten hat keinen Nachfolger
		if (to_delete->get_left() == 0 && to_delete->get_right() == 0)
		{
			delete to_delete;
		}
		//2.Fall: Knoten hat einen Nachfolger
		else if (to_delete->get_left() != 0 || to_delete->get_right() != 0)
		{
			TREE* tmp = to_delete;
			if (to_delete->get_left() != 0) to_delete = to_delete->get_left();
			if (to_delete->get_right() != 0) to_delete = to_delete->get_right();
			delete tmp;
		}
		else
		{
			TREE* tmp = to_delete->get_right();
			while (tmp->get_left() != 0)
			{
				tmp = tmp->get_left();
			}
			current = tmp;
			delete_node(to_delete, tmp->get_content());
			delete to_delete;
		}
	}

}

void print_array(int size, int* array)
{
	cout << "Array wird ausgegeben: " << endl;
	for (int i = 0; i < size; i++)
	{
		cout << "- " << array[i] << endl;
	}
}

int main()
{
	int tree_size = MAX_ELEMENTS;
	int array[MAX_ELEMENTS] = { 2,7,4,13,24,11 };

	print_array(tree_size, array);

	TREE first;
	create_tree(&first, tree_size, array);
	print_tree(&first);

	cout << endl << "Enter zum Beenden!";
	cin.get();
	return 0;
}