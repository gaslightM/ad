#include <iostream>
#include <random>
#include "avlelem1.h"
#include "avltree1.h"

using namespace std;

#define MAX_ELEM 10

int get_random()
{
	random_device random;
	uniform_int_distribution<int> distribution(0, MAX_VALUE);
	return distribution(random);
}

void createTree(avltree1* root)
{
	for (int i = 0; i <= MAX_ELEM; i++)
	{
		root->Insert(root, get_random());
	}
}



int main()
{
	avltree1 tree;
	

	cout << "Baum wird erstellt und bef�llt." << endl;
	createTree(tree&);

	cout << "H�he wird ausgegeben" << endl;
	tree.Print();

	cin.get();
	return 0;
}