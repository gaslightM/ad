#include"avlelem1.h"

#pragma once
#ifndef AVLTREE1

typedef int object;

class avltree1 {
private:
	avlelem1 *root;

	int Max(int a, int b);

	int GetHeight(avlelem1 *elem);

	void UpdateHeight(avlelem1 *elem);

	void Insert(avlelem1* &elem, object o);

	void DeleteTree(avlelem1 *root);

	void CheckRotationRight(avlelem1* &elem);

	void CheckRotationLeft(avlelem1* &elem);

	void Print(avlelem1 *curr);

	void RotateLeft(avlelem1* &a);

	void DoubleRotationLeft(avlelem1* &a);

	void RotateRight(avlelem1* &a);

	void DoubleRotationRight(avlelem1* &a);

public:
	avltree1();

	~avltree1();

	void Insert(object o);	// Wert einf�gen

	void Print();	// Baum ausgeben
};


#endif // !AVLTREE1


