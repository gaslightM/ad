#include "avltree1.h"
#include <iostream>
using namespace std;


avltree1::avltree1() {
	root = 0;
}

avltree1::~avltree1() {
	DeleteTree(root); 
	root = 0;
}

int avltree1::Max(int a, int b) {
	if (a < b) return b;
	else return a;
}

int avltree1::GetHeight(avlelem1 *elem) {
	if (elem == 0) return -1;
	else return elem->height;
}

void avltree1::UpdateHeight(avlelem1 *elem) {
	elem->height = 1 + Max(GetHeight(elem->left),
		GetHeight(elem->right));
}

void avltree1::RotateLeft(avlelem1* &a) {
	avlelem1 *b = a->right;
	a->right = b->left; b->left = a; a = b;
	UpdateHeight(a->left); UpdateHeight(a);
}

void avltree1::DeleteTree(avlelem1* root) 
{
	cout << "Delete Tree!" << endl;
}


void avltree1::RotateRight(avlelem1* &a) {
	avlelem1 *b = a->left;
	a->left = b->right; b->right = a; a = b;
	UpdateHeight(a->right); UpdateHeight(a);
}

void avltree1::DoubleRotationLeft(avlelem1* &a)
{
	RotateRight(a->right);
	RotateLeft(a);
}

void avltree1::DoubleRotationRight(avlelem1* &a)
{
	RotateLeft(a->left);
	RotateRight(a);
}

void avltree1::CheckRotationRight(avlelem1* &elem) {
	if (elem != 0)
	{
		if (elem->left != 0)
		{
			if (GetHeight(elem->left) - GetHeight(elem->right) == 2)
			{
				if (GetHeight(elem->left->right) >
					GetHeight(elem->left->left))
				{
					DoubleRotationRight(elem);
				}
				else RotateRight(elem);
			}
			else UpdateHeight(elem);
		}
		else UpdateHeight(elem);
	}
}

void avltree1::CheckRotationLeft(avlelem1* &elem) {
	if (elem != 0)
	{
		if (elem->right != 0)
		{
			if (GetHeight(elem->right) - GetHeight(elem->left) == 2)
			{
				if (GetHeight(elem->right->left) >
					GetHeight(elem->right->right))
				{
					DoubleRotationLeft(elem);
				}
				else RotateLeft(elem);
			}
			else UpdateHeight(elem);
		}
		else UpdateHeight(elem);
	}
}

void avltree1::Insert(avlelem1* &elem, object o) {
	if (elem == 0) {
		elem = new avlelem1;
		elem->height = 0; elem->val = o;
		elem->left = 0;
		elem->right = 0;
	}
	else {
		if (o <= elem->val) {
			Insert(elem->left, o);
			CheckRotationRight(elem);
		}
		else {
			Insert(elem->right, o);
			CheckRotationLeft(elem);
		}
	}
}

void avltree1::Print()
{
	cout << GetHeight() << endl;
}