#pragma once

#ifndef AVLELEM1

typedef int object;

class avlelem1 
{
public: 
	int height; 
	object val;
	avlelem1 *left, *right;
};


#endif // !AVLELEM1


